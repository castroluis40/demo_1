package com.example.demo.Modules.Project.DTOs;

import com.example.demo.Modules.Client.Entities.Client;
import com.example.demo.Modules.Colaborator.DTOs.ResponseColaboratorDTO;
import com.example.demo.Modules.Colaborator.Entities.Colaborator;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

public class ResponseProjectDTO {
    private Long idProject;

    private String name;

    private Date initialDate;

    private Date endDate;

    private Client client;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<ResponseColaboratorDTO> colaborators;

    public Long getIdProject() {
        return idProject;
    }

    public void setIdProject(Long idProject) {
        this.idProject = idProject;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getInitialDate() {
        return initialDate;
    }

    public void setInitialDate(Date initialDate) {
        this.initialDate = initialDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public List<ResponseColaboratorDTO> getColaborators() {
        return colaborators;
    }

    public void setColaborators(List<ResponseColaboratorDTO> colaborators) {
        this.colaborators = colaborators;
    }
}
