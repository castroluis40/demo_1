package com.example.demo.Modules.Project.Controllers;

import com.example.demo.Core.Errors.CustomErrorType2;
import com.example.demo.Modules.Client.Entities.Client;
import com.example.demo.Modules.Client.ErrorCodes.ClientErrorCodes;
import com.example.demo.Modules.Client.Interfaces.ClientServiceInterface;
import com.example.demo.Modules.Colaborator.Interfaces.ColaboratorServiceInterface;
import com.example.demo.Modules.Colaborator.Mappers.ColaboratorConverter;
import com.example.demo.Modules.Project.Mappers.ProjectConverter;
import com.example.demo.Modules.Project.DTOs.RequestProjectDTO;
import com.example.demo.Modules.Project.DTOs.ResponseProjectDTO;
import com.example.demo.Modules.Project.Entities.Project;
import com.example.demo.Modules.Project.ErrorCodes.ProjectErrorCodes;
import com.example.demo.Modules.Project.Service.ProjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/projects")
public class ProjectController {

    public static final Logger logger = LoggerFactory.getLogger(ProjectController.class);

    ProjectConverter projectConverter = new ProjectConverter();
    ColaboratorConverter colaboratorConverter = new ColaboratorConverter();
    ProjectErrorCodes projectErrorCodes = new ProjectErrorCodes();
    ClientErrorCodes clientErrorCodes = new ClientErrorCodes();

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ClientServiceInterface clientServiceInterface;

    @Autowired
    private ColaboratorServiceInterface colaboratorServiceInterface;

    // ------------ Retrieve all projects ------------
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<ResponseProjectDTO>> getAllProjects() {
        List<Project> projects = projectService.getAllProjects();
        if (projects.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
            // You many decide to return HttpStatus.NOT_FOUND
        }

        List<ResponseProjectDTO> responseProjectDTOs = new ArrayList<ResponseProjectDTO>();

        for (Project project:projects) {
            ResponseProjectDTO responseProjectDTO = projectConverter.convertToResponseProjectDTO(project);
            responseProjectDTO.setColaborators(colaboratorConverter.convertToResponseColaboratorDTOs(colaboratorServiceInterface.getColaboratorsByProjectId(project.getIdProject())));

            responseProjectDTOs.add(responseProjectDTO);
        }

        return new ResponseEntity<List<ResponseProjectDTO>>(responseProjectDTOs, HttpStatus.OK);
    }

    // ------------ Retrieve a project ------------
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getProject(@PathVariable Long id) {
        Project project = projectService.getProject(id);
        if (project == null) {
            return new ResponseEntity(new CustomErrorType2(projectErrorCodes.Validation().get("ProjectNotFound")), HttpStatus.NOT_FOUND);
        }

        ResponseProjectDTO responseProjectDTO = projectConverter.convertToResponseProjectDTO(project);
        responseProjectDTO.setColaborators(colaboratorConverter.convertToResponseColaboratorDTOs(colaboratorServiceInterface.getColaboratorsByProjectId(project.getIdProject())));

        return new ResponseEntity<ResponseProjectDTO>(responseProjectDTO, HttpStatus.OK);
    }

    // ------------ Create a project ------------
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<?> addProject(@RequestBody RequestProjectDTO requestProjectDTO, UriComponentsBuilder ucBuilder) {
        logger.info("Creating Project : {}", requestProjectDTO);

        if (projectService.isProjectExist(requestProjectDTO.getName())) {
            logger.error("Unable to create. A Project with name {} already exist", requestProjectDTO.getName());
            return new ResponseEntity(new CustomErrorType2(projectErrorCodes.Validation().get("ProjectAlreadyExistsByName")), HttpStatus.NOT_FOUND);
        }
        if (!clientServiceInterface.isClientExist(requestProjectDTO.getIdClient())) {
            logger.error("Unable to create. A Project with id {} not exists", requestProjectDTO.getIdClient());
            return new ResponseEntity(new CustomErrorType2(clientErrorCodes.Validation().get("ClientNotFound")), HttpStatus.NOT_FOUND);
        }
        if (requestProjectDTO.getInitialDate().getTime() > requestProjectDTO.getEndDate().getTime()) {
            logger.error("Unable to create. Begin Date could not be greather than End Date");
            return new ResponseEntity(new CustomErrorType2(clientErrorCodes.Validation().get("InvalidDateRanges")), HttpStatus.BAD_REQUEST);
        }

        Project project = projectConverter.convertToProject(requestProjectDTO);
        project.setClient(clientServiceInterface.getClient(requestProjectDTO.getIdClient()));
        projectService.addProject(project);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/{id}").buildAndExpand(project.getIdProject()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    // ------------ Delete a project ------------
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProject(@PathVariable("id") @NotNull Long id) {
        logger.info("Fetching & Project Client with id {}", id);

        Project project = projectService.getProject(id);
        if (project == null) {
            logger.error("Unable to delete. Project with id {} not found.", id);
            return new ResponseEntity(new CustomErrorType2(projectErrorCodes.Validation().get("ProjectNotFoundToDelete")), HttpStatus.NOT_FOUND);
        }
        projectService.deleteProject(id);
        return new ResponseEntity<Client>(HttpStatus.NO_CONTENT);
    }
}
