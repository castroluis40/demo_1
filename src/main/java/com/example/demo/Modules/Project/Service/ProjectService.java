package com.example.demo.Modules.Project.Service;

import com.example.demo.Modules.Client.Entities.Client;
import com.example.demo.Modules.Project.Entities.Project;
import com.example.demo.Modules.Project.Repositories.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;

    // Retrieve all rows from table and populate list with objects
    public List<Project> getAllProjects() {
        return projectRepository.findAll();
    }

    // Retrieves one row from table based on given id
    public Project getProject(Long id) {
        return projectRepository.findById(id).orElse(null);
    }

    // Inserts row into table
    public void addProject(Project project) {
        projectRepository.save(project);
    }

    // Updates row in table
    public void updateProject(Project project) {
        projectRepository.save(project);
    }

    public boolean isProjectExist(String projectName) {
        return findByName(projectName)!=null;
    }

    // Removes row from table
    public void deleteProject(Long id) {
        projectRepository.deleteById(id);
    }

    //region PRIVATE METHODS

    private Project findByName(String name) {
        List<Project> projectList = getAllProjects();
        if (!projectList.isEmpty()) {
            for(Project project : projectList){
                if(project.getName().equalsIgnoreCase(name)){
                    return project;
                }
            }
        }

        return null;
    }

    //endregion
}

