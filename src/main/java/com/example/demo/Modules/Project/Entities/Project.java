package com.example.demo.Modules.Project.Entities;

import com.example.demo.Modules.Client.Entities.Client;
import com.example.demo.Modules.Colaborator.Entities.Colaborator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="project")
public class Project {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    @Column(name="id_project")
    private Long idProject;

    @Column(name="name")
    private String name;

    @Column(name="initial_date")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date initialDate;

    @Column(name="end_date")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date endDate;

    @OneToOne
    @JoinColumn(name = "id_client", referencedColumnName = "id_client")
    private Client client;
/*
    @OneToMany(mappedBy = "id_project")
    List<Colaborator> colaborators;
*/
    public Project() {}

    public Project(Long idProject, String name, String email, Date initialDate, Date endDate, Client client) {
        this.idProject = idProject;
        this.name = name;
        this.initialDate = initialDate;
        this.endDate = endDate;
        this.client = client;
    }

    public Long getIdProject() {
        return idProject;
    }

    public void setIdProject(Long idProject) {
        this.idProject = idProject;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getInitialDate() {
        return initialDate;
    }

    public void setInitialDate(Date initialDate) {
        this.initialDate = initialDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}

