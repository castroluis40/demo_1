package com.example.demo.Modules.Project.Mappers;

import com.example.demo.Core.ModelMapper.ObjectMapperUtils;
import com.example.demo.Modules.Project.DTOs.RequestProjectDTO;
import com.example.demo.Modules.Project.DTOs.ResponseProjectDTO;
import com.example.demo.Modules.Project.Entities.Project;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ProjectConverter {
    @Autowired
    private static ModelMapper modelMapper;

    public RequestProjectDTO convertToRequestProjectDTO(Project project) {
        if (project == null) {
            return null;
        }

        ModelMapper modelMapper = new ModelMapper();
        RequestProjectDTO orderDTO = modelMapper.map(project, RequestProjectDTO.class);

        return orderDTO;
    }

    public List<RequestProjectDTO> convertToRequestProjectDTO(List<Project> projects) {
        List<RequestProjectDTO> resultClientDTOsList = ObjectMapperUtils.mapAll(projects, RequestProjectDTO.class);

        return resultClientDTOsList;
    }

    public Project convertToProject(RequestProjectDTO requestProjectDTO) {
        if (requestProjectDTO == null) {
            return null;
        }

        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(requestProjectDTO, Project.class);
    }

    public List<Project> convertToProject(List<RequestProjectDTO> requestProjectDTOS) {
        return ObjectMapperUtils.mapAll(requestProjectDTOS, Project.class);
    }

    public ResponseProjectDTO convertToResponseProjectDTO(Project project) {
        if (project == null) {
            return null;
        }

        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(project, ResponseProjectDTO.class);
    }

    public List<ResponseProjectDTO> convertToResponseProjectDTO(List<Project> projects) {
        return ObjectMapperUtils.mapAll(projects, ResponseProjectDTO.class);
    }
}

