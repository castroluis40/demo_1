package com.example.demo.Modules.Project.Controllers;

import com.example.demo.Core.Errors.CustomErrorType;
import com.example.demo.Modules.Client.Entities.Client;
import com.example.demo.Modules.Colaborator.Interfaces.ColaboratorServiceInterface;
import com.example.demo.Modules.Project.Entities.Project;
import com.example.demo.Modules.Project.Service.ProjectService;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/projects/{project_id}/colaborators")
public class ProjectColaboratorController {

    public static final Logger logger = LoggerFactory.getLogger(ProjectColaboratorController.class);

    @Autowired
    private ColaboratorServiceInterface colaboratorServiceInterface;

    // ------------ Create a project ------------
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<?> addProject(@PathVariable("project_id") long projectId, @RequestBody Long[] colaborators, UriComponentsBuilder ucBuilder) {
        logger.info("Associating Colaborators to Project : Project {0} - Colaborators {1}", projectId, colaborators);
/*
        if (clientService.isUserExist(client)) {
            logger.error("Unable to create. A User with name {} already exist", user.getName());
            return new ResponseEntity(new CustomErrorType("Unable to create. A User with name " +
                    user.getName() + " already exist."),HttpStatus.CONFLICT);
        }*/
        colaboratorServiceInterface.updateColaboratorsProject(projectId, colaborators);

        HttpHeaders headers = new HttpHeaders();
        //headers.setLocation(ucBuilder.path("/{id}").buildAndExpand(project.getIdProject()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }
/*
    // ------------ Delete a project ------------
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProject(@PathVariable("project_id") long project_id, @PathVariable("colaborator_id") long id) {
        logger.info("Fetching & Project Client with id {}", id);

        Project project = projectService.getProject(id);
        if (project == null) {
            logger.error("Unable to delete. Project with id {} not found.", id);
            return new ResponseEntity(new CustomErrorType("Unable to delete. Project with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        projectService.deleteProject(id);
        return new ResponseEntity<Client>(HttpStatus.NO_CONTENT);
    }*/
}
