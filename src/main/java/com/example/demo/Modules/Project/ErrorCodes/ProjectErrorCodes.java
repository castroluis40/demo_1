package com.example.demo.Modules.Project.ErrorCodes;

import com.example.demo.Core.Exceptions.ErrorCode;
import com.google.common.base.Strings;

import java.util.HashMap;

public class ProjectErrorCodes {
    private HashMap<String, ErrorCode> errorCodesList;

    private static String ModuleCode = Strings.padStart("3", 4, '0');

    public HashMap<String, ErrorCode> Validation() {
        HashMap<String, ErrorCode> list = new HashMap<>();
        list.put("ProjectNotFound", new ErrorCode(getValidationErrorCodeKey(1), "Unable to list. Project Not Found"));
        list.put("ProjectAlreadyExistsByName", new ErrorCode(getValidationErrorCodeKey(2), "Unable to create. Project with the same name already exists"));
        list.put("ProjectAlreadyExistsByEmail", new ErrorCode(getValidationErrorCodeKey(3), "Unable to create. Project the same email already exists"));
        list.put("ProjectNotFoundToDelete", new ErrorCode(getValidationErrorCodeKey(4), "Unable to delete. Project not found"));

        return list;
    }

    public HashMap<String, ErrorCode> Logic() {
        HashMap<String, ErrorCode> list = new HashMap<>();
        list.put("", new ErrorCode(getLogicErrorCodeKey(1), ""));

        return list;
    }

    //region PRIVATE METHODS

    private String getLogicErrorCodeKey(int code){
        String error = String.format("%04d", code);

        return  "" + ErrorCode.getLogicError() + ModuleCode + code;
    }

    private String getValidationErrorCodeKey(int code){
        String error = String.format("%04d", code);

        return  "" + ErrorCode.getValidationError() + ModuleCode + code;
    }

    //endregion
}
