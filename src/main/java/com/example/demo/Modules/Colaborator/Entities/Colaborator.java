package com.example.demo.Modules.Colaborator.Entities;

import com.example.demo.Modules.Project.Entities.Project;

import javax.persistence.*;

@Entity
@Table(name="colaborator")
public class Colaborator {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    @Column(name="id_colaborator")
    private Long idColaborator;

    @Column(name="name")
    private String name;

    @Column(name="email")
    private String email;

    @OneToOne
    @JoinColumn(name = "id_project", referencedColumnName = "id_project")
    private Project project;

    public Colaborator() {}

    public Colaborator(Long idColaborator, String name, String email, Project project) {
        this.idColaborator = idColaborator;
        this.name = name;
        this.email = email;
        this.project = project;
    }

    public Long getIdColaborator() {
        return idColaborator;
    }

    public void setIdColaborator(Long idColaborator) {
        this.idColaborator = idColaborator;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}

