package com.example.demo.Modules.Colaborator.Mappers;

import com.example.demo.Core.ModelMapper.ObjectMapperUtils;
import com.example.demo.Modules.Colaborator.DTOs.ResponseColaboratorDTO;
import com.example.demo.Modules.Colaborator.Entities.Colaborator;
import com.example.demo.Modules.Project.DTOs.RequestProjectDTO;
import com.example.demo.Modules.Project.DTOs.ResponseProjectDTO;
import com.example.demo.Modules.Project.Entities.Project;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ColaboratorConverter {
    public ResponseColaboratorDTO convertToResponseColaboratorDTO(Colaborator colaborator) {
        if (colaborator == null) {
            return null;
        }

        ModelMapper modelMapper = new ModelMapper();

        ResponseColaboratorDTO responseColaboratorDTO = modelMapper.map(colaborator, ResponseColaboratorDTO.class);

        return responseColaboratorDTO;
    }

    public List<ResponseColaboratorDTO> convertToResponseColaboratorDTOs(List<Colaborator> colaborators) {
        List<ResponseColaboratorDTO> responseColaboratorDTOs = ObjectMapperUtils.mapAll(colaborators, ResponseColaboratorDTO.class);

        return responseColaboratorDTOs;
    }
}