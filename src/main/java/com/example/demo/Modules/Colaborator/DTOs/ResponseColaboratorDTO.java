package com.example.demo.Modules.Colaborator.DTOs;

import com.example.demo.Modules.Project.Entities.Project;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ResponseColaboratorDTO {
    private Long idColaborator;

    private String name;

    private String email;

    public Long getIdColaborator() {
        return idColaborator;
    }

    public void setIdColaborator(Long idColaborator) {
        this.idColaborator = idColaborator;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
