package com.example.demo.Modules.Colaborator.Controllers;

import com.example.demo.Core.Errors.CustomErrorType;
import com.example.demo.Core.Errors.CustomErrorType2;
import com.example.demo.Modules.Client.Entities.Client;
import com.example.demo.Modules.Client.ErrorCodes.ClientErrorCodes;
import com.example.demo.Modules.Colaborator.Entities.Colaborator;
import com.example.demo.Modules.Colaborator.ErrorCodes.ColaboratorErrorCodes;
import com.example.demo.Modules.Colaborator.Service.ColaboratorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/colaborators")
public class ColaboratorController {

    public static final Logger logger = LoggerFactory.getLogger(ColaboratorController.class);

    ColaboratorErrorCodes colaboratorErrorCodes = new ColaboratorErrorCodes();

    @Autowired
    private ColaboratorService colaboratorService;

    // ------------ Retrieve all colaborators ------------
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<Colaborator>> getAllColaborators() {
        List<Colaborator> colaborators = colaboratorService.getAllColaborators();
        if (colaborators.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
            // You many decide to return HttpStatus.NOT_FOUND
        }

        return new ResponseEntity<List<Colaborator>>(colaborators, HttpStatus.OK);
    }

    // ------------ Retrieve a colaborator ------------
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getColaborator(@PathVariable Long id) {
        Colaborator colaborators = colaboratorService.getColaborator(id);
        if (colaborators == null) {
            return new ResponseEntity(new CustomErrorType2(colaboratorErrorCodes.Validation().get("ColaboratorNotFound")), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Colaborator>(colaborators, HttpStatus.OK);
    }

    // ------------ Create a colaborator ------------
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<?> addColaborator(@RequestBody Colaborator colaborator, UriComponentsBuilder ucBuilder) {
        logger.info("Creating Colaborator : {}", colaborator);

        if (colaboratorService.isNameExist(colaborator)) {
            logger.error("Unable to create. A Colaborator with name {} already exist", colaborator.getName());
            return new ResponseEntity(new CustomErrorType2(colaboratorErrorCodes.Validation().get("ColaboratorAlreadyExistsByName")), HttpStatus.NOT_FOUND);
        }
        if (colaboratorService.isEmailExist(colaborator)) {
            logger.error("Unable to create. A Colaborator with email {} already exist", colaborator.getEmail());
            return new ResponseEntity(new CustomErrorType2(colaboratorErrorCodes.Validation().get("ColaboratorAlreadyExistsByEmail")), HttpStatus.NOT_FOUND);
        }
        colaboratorService.addColaborator(colaborator);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/{id}").buildAndExpand(colaborator.getIdColaborator()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    // ------------ Delete a colaborator ------------
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteColaborator(@PathVariable("id") @NotNull Long id) {
        logger.info("Fetching & Colaborator Client with id {}", id);

        Colaborator colaborator = colaboratorService.getColaborator(id);
        if (colaborator == null) {
            logger.error("Unable to delete. Colaborator with id {} not found.", id);
            return new ResponseEntity(new CustomErrorType2(colaboratorErrorCodes.Validation().get("ColaboratorNotFoundToDelete")), HttpStatus.NOT_FOUND);
        }
        colaboratorService.deleteColaborator(id);
        return new ResponseEntity<Client>(HttpStatus.NO_CONTENT);
    }
}
