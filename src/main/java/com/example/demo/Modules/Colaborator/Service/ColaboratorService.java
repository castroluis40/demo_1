package com.example.demo.Modules.Colaborator.Service;

import com.example.demo.Modules.Client.Entities.Client;
import com.example.demo.Modules.Colaborator.Entities.Colaborator;
import com.example.demo.Modules.Colaborator.Interfaces.ColaboratorServiceInterface;
import com.example.demo.Modules.Colaborator.Repositories.ColaboratorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ColaboratorService implements ColaboratorServiceInterface {
    @Autowired
    private ColaboratorRepository colaboratorRepository;

    @Override
    // Retrieve all rows from table and populate list with objects
    public List<Colaborator> getAllColaborators() {
        List<Colaborator> colaborators = new ArrayList<>();
        return colaboratorRepository.findAll();
    }

    @Override
    // Retrieves one row from table based on given id
    public Colaborator getColaborator(Long id) {
        return colaboratorRepository.findById(id).orElse(null);
    }

    @Override
    public Colaborator getColaboratorByProjectId(Long projectId) {
        return findByProjectId(projectId);
    }

    @Override
    public List<Colaborator> getColaboratorsByProjectId(Long projectId) {
        return findAllByProjectId(projectId);
    }

    @Override
    // Inserts row into table
    public void addColaborator(Colaborator colaborator) {
        colaboratorRepository.save(colaborator);
    }

    @Override
    // Updates rows in table
    public void updateColaborators(List<Colaborator> colaborators){
        for (Colaborator colaborator:colaborators) {
            colaboratorRepository.save(colaborator);
        }
    }

    @Override
    // Updates row in table
    public void updateColaborator(Colaborator colaborator) {
        colaboratorRepository.save(colaborator);
    }

    @Override
    // Updates field in row of the table
    public void updateColaboratorProject(Long projectID, Long colaboratorID) {
        colaboratorRepository.updateColaboratorProject(colaboratorID, projectID);
    }

    @Override
    // Updates field in rows of the table
    public void updateColaboratorsProject(Long projectID, Long[] colaboratorsIDList){
        for (Long colaboratorID:colaboratorsIDList) {
            colaboratorRepository.updateColaboratorProject(projectID, colaboratorID);
        }
    }

    @Override
    // Removes row from table
    public void deleteColaborator(Long id) {
        colaboratorRepository.deleteById(id);
    }

    public boolean isNameExist(Colaborator colaborator) {
        return findByName(colaborator.getName())!=null;
    }

    public boolean isEmailExist(Colaborator colaborator) {
        return findByEmail(colaborator.getEmail())!=null;
    }

    //region PRIVATE METHODS

    private Colaborator findByName(String name) {
        List<Colaborator> clientList = getAllColaborators();
        if (!clientList.isEmpty()) {
            for(Colaborator colaborator : clientList){
                if(colaborator.getName().equalsIgnoreCase(name)){
                    return colaborator;
                }
            }
        }

        return null;
    }

    private Colaborator findByEmail(String email) {
        List<Colaborator> colaboratorList = getAllColaborators();
        if (!colaboratorList.isEmpty()) {
            for(Colaborator colaborator : colaboratorList){
                if(colaborator.getEmail().equalsIgnoreCase(email)){
                    return colaborator;
                }
            }
        }

        return null;
    }

    private Colaborator findByProjectId(Long projectId) {
        List<Colaborator> colaboratorList = getAllColaborators();
        if (!colaboratorList.isEmpty()) {
            for(Colaborator colaborator : colaboratorList){
                if(colaborator.getProject() != null){
                    if(colaborator.getProject().getIdProject() == projectId){
                        return colaborator;
                    }
                }
            }
        }

        return null;
    }

    private List<Colaborator> findAllByProjectId(Long projectId) {
        List<Colaborator> colaboratorList = getAllColaborators();
        List<Colaborator> returnList = new ArrayList<>();
        if (!colaboratorList.isEmpty()) {
            for(Colaborator colaborator : colaboratorList){
                if(colaborator.getProject() != null){
                    if(colaborator.getProject().getIdProject() == projectId){
                        returnList.add(colaborator);
                    }
                }
            }

            return returnList;
        }

        return null;
    }

    //endregion
}

