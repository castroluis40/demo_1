package com.example.demo.Modules.Colaborator.Interfaces;

import com.example.demo.Modules.Colaborator.Entities.Colaborator;

import java.util.ArrayList;
import java.util.List;

public interface ColaboratorServiceInterface {
    public List<Colaborator> getAllColaborators();

    // Retrieves one row from table based on given id
    public Colaborator getColaborator(Long id);

    // Retrieves one row from table based on given project_id
    public Colaborator getColaboratorByProjectId(Long id);

    // Retrieves one row from table based on given project_id
    public List<Colaborator> getColaboratorsByProjectId(Long id);

    // Inserts row into table
    public void addColaborator(Colaborator colaborator);

    // Updates row in table
    public void updateColaborators(List<Colaborator> colaborators);

    // Updates row in table
    public void updateColaborator(Colaborator colaborator);

    // Updates field in row of the table
    public void updateColaboratorProject(Long projectID, Long colaboratorID);

    // Updates field in rows of the table
    public void updateColaboratorsProject(Long projectID, Long[] colaboratorsIDList);

    // Removes row from table
    public void deleteColaborator(Long id);
}
