package com.example.demo.Modules.Colaborator.Repositories;

import com.example.demo.Modules.Colaborator.Entities.Colaborator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Table;

public interface ColaboratorRepository extends JpaRepository<Colaborator,Long> {
    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value="UPDATE colaborator SET id_project = :projectId WHERE id_colaborator = :colaboratorId", nativeQuery=true)
    void updateColaboratorProject(@Param("projectId") long projectId, @Param("colaboratorId") long colaboratorId);
}
