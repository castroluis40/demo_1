package com.example.demo.Modules.Client.Entities;

import javax.persistence.*;

@Entity
@Table(name="client")
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id_client")
    private Long idClient;

    @Column(name="name")
    private String name;

    @Column(name="email")
    private String email;

    @Column(name="nationality")
    private String nationality;

    @Column(name="contact_name")
    private String contactName;

    @Column(name="contact_email")
    private String contactEmail;

    @Column(name="contact_mobile")
    private String contactMobile;

    public Client() {}

    public Client(String name, String email, String nationality, String contactName, String contactEmail, String contactMobile) {
        this.name = name;
        this.email = email;
        this.nationality = nationality;
        this.contactName = contactName;
        this.contactEmail = contactEmail;
        this.contactMobile = contactMobile;
    }

    public Client(Long id, String name, String email, String nationality, String contactName, String contactEmail, String contactMobile) {
        this.idClient = id;
        this.name = name;
        this.email = email;
        this.nationality = nationality;
        this.contactName = contactName;
        this.contactEmail = contactEmail;
        this.contactMobile = contactMobile;
    }

    public Long getIdClient() {
        return idClient;
    }

    public void setIdClient(Long idClient) {
        this.idClient = idClient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactMobile() {
        return contactMobile;
    }

    public void setContactMobile(String contactMobile) {
        this.contactMobile = contactMobile;
    }
}