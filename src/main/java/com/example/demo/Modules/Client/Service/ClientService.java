package com.example.demo.Modules.Client.Service;

import com.example.demo.Modules.Client.Interfaces.ClientServiceInterface;
import com.example.demo.Modules.Client.MemoryData.ClientMemoryData;
import com.example.demo.Modules.Client.Entities.Client;
import com.example.demo.Modules.Client.Repositories.ClientRepository;
import com.example.demo.Modules.Project.Entities.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientService implements ClientServiceInterface {
    @Autowired
    private ClientRepository clientRepository;

    @Override
    // Retrieve all rows from table and populate list with objects
    public List<Client> getAllClients() {
        return clientRepository.findAll();
    }

    @Override
    // Retrieve all clients stored in memory
    public List<Client> getAllClientsInMemory() {
        ClientMemoryData clientMemoryData = new ClientMemoryData();

        return clientMemoryData.getAllClientsMemoryData();
    }

    @Override
    // Retrieves one row from table based on given id
    public Client getClient(Long id) {
        return clientRepository.findById(id).orElse(null);
    }

    @Override
    // Retrieves one client stored in memory by id
    public Client getClientInMemory(Long id) {
        ClientMemoryData clientMemoryData = new ClientMemoryData();

        return clientMemoryData.getClientMemoryDataById(id);
    }

    @Override
    public boolean isNameExist(Client client) {
        return findByName(client.getName())!=null;
    }

    @Override
    public boolean isEmailExist(Client client) {
        return findByEmail(client.getEmail())!=null;
    }

    @Override
    public boolean isClientExist(Long idClient) {
        return findById(idClient)!=null;
    }

    @Override
    // Inserts row into table
    public void addClient(Client client) {
        clientRepository.save(client);
    }

    @Override
    // Updates row in table
    public void updateClient(Client client) {
        clientRepository.save(client);
    }

    @Override
    // Removes row from table
    public void deleteClient(Long id) {
        clientRepository.deleteById(id);

        /* Not needed with cascade all
        ContactPerson contactPerson = contactPersonRepository.findById(id).orElse(null);

        if(contactPerson != null){
            contactPersonRepository.deleteById(id);
        }*/
    }

    //region PRIVATE METHODS

    private Client findByName(String name) {
        List<Client> clientList = getAllClients();
        if (!clientList.isEmpty()) {
            for(Client client : clientList){
                if(client.getName().equalsIgnoreCase(name)){
                    return client;
                }
            }
        }

        return null;
    }

    private Client findByEmail(String email) {
        List<Client> clientList = getAllClients();
        if (!clientList.isEmpty()) {
            for(Client client : clientList){
                if(client.getEmail().equalsIgnoreCase(email)){
                    return client;
                }
            }
        }

        return null;
    }

    private Client findById(Long id) {
        List<Client> clientList = getAllClients();
        if (!clientList.isEmpty()) {
            for(Client client : clientList){
                if(client.getIdClient() == id){
                    return client;
                }
            }
        }

        return null;
    }

    //endregion
}

