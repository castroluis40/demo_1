package com.example.demo.Modules.Client.Mappers;

import com.example.demo.Core.ModelMapper.ObjectMapperUtils;
import com.example.demo.Modules.Client.DTOs.ResultClientDTO;
import com.example.demo.Modules.Client.Entities.Client;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ClientConverter {
    @Autowired
    private static ModelMapper modelMapper;

    public ResultClientDTO convertToClientDTO(Client client) {
        if (client == null) {
            return null;
        }

        ModelMapper modelMapper = new ModelMapper();
        ResultClientDTO orderDTO = modelMapper.map(client, ResultClientDTO.class);

        return orderDTO;
    }

    public List<ResultClientDTO> convertToClientDTO(List<Client> clients) {
        List<ResultClientDTO> resultClientDTOsList = ObjectMapperUtils.mapAll(clients, ResultClientDTO.class);

        return resultClientDTOsList;
    }
}

