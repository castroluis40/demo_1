package com.example.demo.Modules.Client.Controllers;

import com.example.demo.Core.Errors.CustomErrorType2;
import com.example.demo.Core.Exceptions.ClassNotFoundException;
import com.example.demo.Modules.Client.DTOs.ResultClientDTO;
import com.example.demo.Modules.Client.Mappers.ClientConverter;
import com.example.demo.Modules.Client.Entities.Client;
import com.example.demo.Modules.Client.ErrorCodes.ClientErrorCodes;
import com.example.demo.Modules.Client.Service.ClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/clients")
public class ClientController {

    public static final Logger logger = LoggerFactory.getLogger(ClientController.class);

    ClientConverter clientMappers = new ClientConverter();
    ClientErrorCodes clientErrors = new ClientErrorCodes();

    @Autowired
    private ClientService clientService;

    // ------------ Retrieve all clients ------------
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<Client>> getAllClients() {
        List<Client> clients = clientService.getAllClients();
        if (clients.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
            // You many decide to return HttpStatus.NOT_FOUND
        }

        return new ResponseEntity<List<Client>>(clients, HttpStatus.OK);
    }

    // ------------ Retrieve a client ------------
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getClient(@PathVariable Long id) {
        Client clients = clientService.getClient(id);
        if (clients == null) {
            throw new ClassNotFoundException(clientErrors.Validation().get("ClientNotFound"));
            /*
            return new ResponseEntity(new CustomErrorType("Client with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);*/
        }
        return new ResponseEntity<Client>(clients, HttpStatus.OK);
    }

    // ------------ Retrieve all clients in memory ------------
    @RequestMapping(value = "/memory", method = RequestMethod.GET)
    public ResponseEntity<List<ResultClientDTO>> getAllClientsInMemory() {
        List<ResultClientDTO> resultClientsDTOs = clientMappers.convertToClientDTO(clientService.getAllClientsInMemory());
        if (resultClientsDTOs.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
            // You many decide to return HttpStatus.NOT_FOUND
        }

        return new ResponseEntity<List<ResultClientDTO>>(resultClientsDTOs, HttpStatus.OK);
    }

    // ------------ Retrieve a client in memory ------------
    @RequestMapping(value = "/memory/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getClientInMemory(@PathVariable Long id) {
        ResultClientDTO resultClientDTO = clientMappers.convertToClientDTO(clientService.getClientInMemory(id));
        if (resultClientDTO == null) {
            return new ResponseEntity(new CustomErrorType2(clientErrors.Validation().get("ClientNotFound")), HttpStatus.NOT_FOUND);
            /*
            return new ResponseEntity(new CustomErrorType("Client with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);*/
        }
        return new ResponseEntity<ResultClientDTO>(resultClientDTO, HttpStatus.OK);
    }

    // ------------ Create a client ------------
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<?> addClient(@Valid @RequestBody Client client, UriComponentsBuilder ucBuilder, WebRequest request) {
        logger.info("Creating Client : {}", client);

        if (clientService.isNameExist(client)) {
            logger.error("Unable to create. A Client with name {} already exist", client.getName());
            return new ResponseEntity(new CustomErrorType2(clientErrors.Validation().get("ClientAlreadyExistsByName")), HttpStatus.NOT_FOUND);
            /*return new ResponseEntity(new CustomErrorType("Unable to create. A Client with name " +
                    client.getName() + " already exist."),HttpStatus.CONFLICT);*/
        }
        if (clientService.isEmailExist(client)) {
            logger.error("Unable to create. A Client with email {} already exist", client.getEmail());
            return new ResponseEntity(new CustomErrorType2(clientErrors.Validation().get("ClientAlreadyExistsByEmail")), HttpStatus.NOT_FOUND);
            /*return new ResponseEntity(new CustomErrorType("Unable to create. A Client with email " +
                    client.getEmail() + " already exist."),HttpStatus.CONFLICT);*/
        }
        clientService.addClient(client);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(URI.create(String.format(request.getDescription(false) + "/%d", client.getIdClient())));
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    // ------------ Delete a client ------------
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteClient(@PathVariable("id") Long id) {
        logger.info("Fetching & Deleting Client with id {}", id);

        ResultClientDTO client = clientMappers.convertToClientDTO(clientService.getClient(id));
        if (client == null) {
            logger.error("Unable to delete. Client with id {} not found.", id);
            return new ResponseEntity(new CustomErrorType2(clientErrors.Validation().get("ClientNotFoundToDelete")), HttpStatus.NOT_FOUND);
        }
        clientService.deleteClient(id);
        return new ResponseEntity<Client>(HttpStatus.NO_CONTENT);
    }
}
