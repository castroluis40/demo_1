package com.example.demo.Modules.Client.Interfaces;

import com.example.demo.Modules.Client.Entities.Client;
import com.example.demo.Modules.Client.MemoryData.ClientMemoryData;
import com.example.demo.Modules.Colaborator.Entities.Colaborator;

import java.util.List;

public interface ClientServiceInterface {
    // Retrieve all rows from table and populate list with objects
    public List<Client> getAllClients();

    // Retrieve all clients stored in memory
    public List<Client> getAllClientsInMemory();

    // Retrieves one row from table based on given id
    public Client getClient(Long id);

    // Retrieves one client stored in memory by id
    public Client getClientInMemory(Long id);

    public boolean isNameExist(Client client);

    public boolean isEmailExist(Client client);

    public boolean isClientExist(Long idClient);

    // Inserts row into table
    public void addClient(Client client);

    // Updates row in table
    public void updateClient(Client client);

    // Removes row from table
    public void deleteClient(Long id);
}
