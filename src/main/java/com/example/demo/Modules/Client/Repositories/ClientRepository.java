package com.example.demo.Modules.Client.Repositories;

import com.example.demo.Modules.Client.Entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client,Long> {
}
