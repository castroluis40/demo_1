package com.example.demo.Modules.Client.MemoryData;

import com.example.demo.Modules.Client.Entities.Client;

import java.util.ArrayList;
import java.util.List;

public class ClientMemoryData {
    public ClientMemoryData() {
    }

    private static List<Client> GetAllClientsMemoryData() {
        List<Client> listClientsMemoryData = GetMemoryData();

        return listClientsMemoryData;
    }

    private static Client GetClientMemoryDataById(Long id) {
        List<Client> listClientsMemoryData = GetMemoryData();

        return listClientsMemoryData.stream().filter(carnet -> id.equals(carnet.getIdClient())).findFirst().orElse(null);
    }

    private static List<Client> GetMemoryData(){
        Client client1 = new Client((long)1, "Sonae", "sonae@sonae.com", "pt", "João Azevedo", "joaoazevedo@sonae.com", "258000001");
        Client client2 = new Client((long)2,"Farfetch", "farfetch@farfetch.com", "pt", "Luís Amaral", "luisamaral@farfetch.com", "258000002");
        Client client3 = new Client((long)3,"Ferrari", "ferrari@ferrari.com", "de", "Benedikt Benno", "benediktbenno@ferrari.com", "258000003");
        Client client4 = new Client((long)4,"MotaAmaral", "motaAmaral@motaAmaral.com", "pt", "Nuno Pinto", "nunopinto@motaAmaral.com", "258000004");
        Client client5 = new Client((long)5,"VinhosDoDouro", "vinhosdodouro@vinhosDoDouro.com", "pt", "Joana Duarte", "joanaduarte@vinhosDoDouro.com", "258000005");

        List<Client> listClientsMemoryData = new ArrayList<Client>();

        listClientsMemoryData.add(client1);
        listClientsMemoryData.add(client2);
        listClientsMemoryData.add(client3);
        listClientsMemoryData.add(client4);
        listClientsMemoryData.add(client5);

        return  listClientsMemoryData;
    }

    public List<Client> getAllClientsMemoryData() {
        return GetAllClientsMemoryData();
    }

    public Client getClientMemoryDataById(Long id) {
        return GetClientMemoryDataById(id);
    }
}
