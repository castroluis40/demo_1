package com.example.demo.Modules.Client.ErrorCodes;

import com.example.demo.Core.Exceptions.ErrorCode;
import com.google.common.base.Strings;

import java.util.*;

public class ClientErrorCodes {
    private HashMap<String, ErrorCode> errorCodesList;

    private static String ModuleCode = Strings.padStart("1", 4, '0');

    public HashMap<String, ErrorCode> Validation() {
        HashMap<String, ErrorCode> list = new HashMap<>();
        list.put("ClientNotFound", new ErrorCode(getValidationErrorCodeKey(1), "Unable to list. Client Not Found"));
        list.put("ClientAlreadyExistsByName", new ErrorCode(getValidationErrorCodeKey(2), "Unable to create. Client with the same name already exists"));
        list.put("ClientAlreadyExistsByEmail", new ErrorCode(getValidationErrorCodeKey(3), "Unable to create. Client the same email already exists"));
        list.put("ClientNotFoundToDelete", new ErrorCode(getValidationErrorCodeKey(4), "Unable to delete. Client not found"));
        list.put("InvalidDateRanges", new ErrorCode(getValidationErrorCodeKey(4), "Unable to create. Begin Date could not be greather than End Date"));

        return list;
    }

    public HashMap<String, ErrorCode> Logic() {
        HashMap<String, ErrorCode> list = new HashMap<>();
        list.put("", new ErrorCode(getLogicErrorCodeKey(1), ""));

        return list;
    }

    //region PRIVATE METHODS

    private String getLogicErrorCodeKey(int code){
        String error = String.format("%04d", code);

        return  "" + ErrorCode.getLogicError() + ModuleCode + error;
    }

    private String getValidationErrorCodeKey(int code){
        String error = String.format("%04d", code);

        return  "" + ErrorCode.getValidationError() + ModuleCode + error;
    }

    //endregion
}
