package com.example.demo.Core.Errors;

import com.example.demo.Core.Exceptions.ErrorCode;

public class CustomErrorType2 {

    private String errorMessage;
    private String errorCode;

    public CustomErrorType2(ErrorCode error){
        this.errorCode = error.getCode();
        this.errorMessage = error.getMessage();
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}
