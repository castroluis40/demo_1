package com.example.demo.Core.Exceptions;

import java.util.Date;

public class ErrorDetails {
    private Date timestamp;
    private String message;
    private String errorCode;
    private String location;

    public ErrorDetails(Date timestamp, String errorCode, String errorMessage, String location) {
        super();
        this.timestamp = timestamp;
        this.errorCode = errorCode;
        this.message = errorMessage;
        this.location = location;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        return message;
    }

    public String getLocation() {
        return location;
    }

}