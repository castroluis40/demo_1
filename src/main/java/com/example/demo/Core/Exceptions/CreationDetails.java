package com.example.demo.Core.Exceptions;

import java.util.Date;

public class CreationDetails {
    private Date timestamp;
    private String location;

    public CreationDetails(Date timestamp, String location) {
        super();
        this.timestamp = timestamp;
        this.location = location;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getLocation() {
        return location;
    }

}