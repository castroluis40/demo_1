package com.example.demo.Core.Exceptions;

import java.util.Date;

public class ErrorCode {
    private String Code;
    private String Message;
    private static short LogicError = 2;
    private static short ValidationError = 3;

    public ErrorCode(String code, String message) {
        super();
        this.Code = code;
        this.Message = message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public static short getLogicError() {
        return LogicError;
    }

    public static short getValidationError() {
        return ValidationError;
    }
}