package com.example.demo.Core.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ClassNotFoundException extends RuntimeException {

    public ClassNotFoundException(ErrorCode exception) {
        super(exception.getCode() + ":" + exception.getMessage());
    }
}